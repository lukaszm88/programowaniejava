import java.util.Scanner;

public class Kolo {
    public static void main (String[] args) {
        double pi = 3.14;
        double r;
        double pole = 0;
        double dlugosc = 0;
        //deklaruje zmienna do pobrania danych z klawy
        Scanner klawiatura = new Scanner(System.in);
        //pobieram wartosc
        System.out.println("Podaj promień");

        r = klawiatura.nextDouble();

        if (r > 0) {
            //Obliczam pole okręgu
            pole = pi * (r*r);
            //Obliczam długość okręgu
            dlugosc = 2* pi * r;
        } else {
            if (r <= 0) {
                System.out.println("Promień nie moze byc ujemny bądz równy 0");
                return;
            }
        }

        //wyswietlam
        System.out.println("pole okręgu wynosi: " + pole);
        System.out.println("długość okręgu wynosi: " + dlugosc);
    }
}
