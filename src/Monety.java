/*      Napisz program, który na początku wczyta do zmiennej double pewną liczbę, która będzie
        reprezentowała kwotę w polskich złotówkach. Zadanie polega na napisaniu ile monet
        poszczególnych nominałów potrzeba aby rozmienić całą kwotę. Na przykład dla 11 złotych
        wynikiem powinno być: 2 pięciozłotówki, 1 jednozłotówka i żadnych innych monet. */

import java.util.Scanner;

public class Monety {

    public static void main (String[] args) {

        double kwota;


        Scanner klawiatura = new Scanner(System.in);

        System.out.println("Podaj kwotę");

        kwota = klawiatura.nextDouble();

        System.out.println("Podano kwotę : " + kwota + " pln");

    }
}
