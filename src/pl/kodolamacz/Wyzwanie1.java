package pl.kodolamacz;
import java.util.Scanner;

public class Wyzwanie1 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Proszę podaj swoje imię:");
	    String imie = scanner.nextLine();
        System.out.println("Proszę podaj swoje nazwisko:");
	    String nazwisko = scanner.nextLine();
	    scanner.close();

	    System.out.println("Witaj " +imie + " " + nazwisko);
    }
}
