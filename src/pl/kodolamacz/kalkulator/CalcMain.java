package pl.kodolamacz.kalkulator;

import java.util.Scanner;

public class CalcMain {

    public static void main(String[] args) {


        Scanner klawiatura = new Scanner(System.in);

        int licznik1;
        int licznik2;
        int mianowinik1;
        int mianowinik2;
        String znak;
        int petla = 1;
        String operacja;


        while (petla > 0) {
            System.out.println("Wprowadź licnzik pierwszej liczby");
            licznik1 = klawiatura.nextInt();
            System.out.println("Wprowadź mianownik pierwszej liczby");
            mianowinik1 = klawiatura.nextInt();
            System.out.println("Wprowadź operację arytmetyczną: +, -, *, /");
            znak = klawiatura.next();
            System.out.println("Wprowadź licnzik drugiej liczby");
            licznik2 = klawiatura.nextInt();
            System.out.println("Wprowadź mianownik drugiej liczby");
            mianowinik2 = klawiatura.nextInt();


            Calc calc1 = new Calc();
            calc1.licznik1 = licznik1;
            calc1.mianownik1 = mianowinik1;
            calc1.licznik2 = licznik2;
            calc1.mianownik2 = mianowinik2;

            switch (znak) {
                case "*":
                    calc1.pomnoz();
                    break;
                case "/":
                    calc1.podziel();
                    break;
                case "+":
                    calc1.dodaj();
                    break;
                case "-":
                    calc1.odejmij();
                    break;
            }

            calc1.wynik();

            System.out.println("Czy chcesz powtórzyć obliczenia? (t/n)");
            operacja = klawiatura.next();
            if (operacja.equals("t")) {
                continue;
            } else {
                if (operacja.equals("n")) {
                    break;
                }
            }
        }
    }
}
