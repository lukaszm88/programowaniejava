package pl.kodolamacz.math;

import java.util.Scanner;

public class SimpleCalculator {

    public static void main(String[] args) {

        var scanner = new Scanner(System.in);

        System.out.println("Proszę podaj w oddzielnych linijkach jakąś liczbę, operację matematyczną +,-,*,/ a następnie kolejną liczbę:");

        String runAgain;
        do {

            var numerator = scanner.nextInt();
            scanner.next();
            var denominator = scanner.nextInt();
            scanner.nextLine();
            var x = new Fraction(numerator, denominator);

            var operation = scanner.nextLine();

            numerator = scanner.nextInt();
            scanner.next();
            denominator = scanner.nextInt();
            scanner.nextLine();
            var y = new Fraction(numerator, denominator);

            Fraction result;
            if (operation.equals("+")) {
                result = x.add(y);
            } else if (operation.equals("-")) {
                result = x.subtract(y);
            } else if (operation.equals("*")) {
                result = x.multiply(y);
            } else if (operation.equals("/")) {
                result = x.divide(y);
            } else {
                System.out.println("Nieznana operacja!");
                result = new Fraction(0);
            }
            System.out.println("Twój wynik to: " + result.getFractionAsString());

            System.out.println("Chcesz wykonać kolejne działanie? Wpisz literę t lub n.");
            runAgain = scanner.nextLine();

        } while (runAgain.equals("t"));

        scanner.close();
    }

}