package pl.kodolamacz;

import pl.kodolamacz.myclasses.Person;

public class MyThirdJavaApplication {

    public static void main(String[] args) {
        Person person1 = new Person(); // tworzymy obiekt
        person1.forename = "Jan"; // przypisujemy polu imię wartość
        person1.surname = "Kowalski";
        person1.age = 35;
        person1.introduceYourself(); // wywołujemy metodę
        person1.growOld(5); // metoda z parametrem
        person1.introduceYourself(); // poprzednia metoda po zmianie parametrów

        Person person2 = new Person("Tomasz", "Nowak", 56); // konstruktor z parametrami
        person2.introduceYourself();
        person2.growOld(5); // metoda z parametrem
        person2.introduceYourself(); // poprzednia metoda po zmianie parametrów
    }

}
