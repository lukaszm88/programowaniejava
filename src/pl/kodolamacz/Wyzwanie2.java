package pl.kodolamacz;

import java.util.Scanner;

public class Wyzwanie2 {

    public static void main(String[] args) {

        double liczba1;
        double liczba2;
        String znak;
        double wynik = 0;
        int petla = 1;
        String operacja;

        Scanner klawiatura = new Scanner(System.in);

        while (petla > 0) {

            System.out.println("Wprowadź liczbę");
            liczba1 = klawiatura.nextDouble();
            System.out.println("Wprowadz znak operacji arytmetycznej");
            znak = klawiatura.next();
            System.out.println("Wprowadź liczbę");
            liczba2 = klawiatura.nextDouble();

            if (znak.equals("+")) {
                wynik = liczba1 + liczba2;
            } else if (znak.equals("-")) {
                wynik = liczba1 - liczba2;
            } else if (znak.equals("*")) {
                wynik = liczba1 * liczba2;
            } else if (znak.equals("/")) {
                wynik = liczba1 / liczba2;
            } else if (znak.equals("%")) {
                wynik = liczba1 % liczba2;
            }

            System.out.println("Wynik wynosi: " + wynik);

            System.out.println("Czy chcesz powtórzyć obliczenia? (t/n)");
            operacja = klawiatura.next();
            if (operacja.equals("t")) {
                continue;
            } else {
                if (operacja.equals("n")) {
                    break;
                }
            }
        }
    }
}

