package pl.kodolamacz.myclasses;

public class Person {

    public String forename;
    public String surname;
    public int age;

    /**
     * Konstruktor domyślny
     */
    public Person() {

    }

    /**
     * Konstruktor z trzema parametrami
     */
    public Person(String forename, String surname, int age) {
        this.forename = forename;
        this.surname = surname;
        this.age = age;
    }

    public void introduceYourself() {
        System.out.println("Nazywam się " + forename + " " + surname + ". Mam " + age + " lat.");
    }

    public int growOld(int age) {
        this.age += age;
        return this.age;
    }

}
