package pl.kodolamacz;

import java.util.Scanner;

public class Wyzwanie2v2 {
    public static void main (String[] args) {

        int liczba1;
        int liczba2;
        int znak = 0;
        float wynik = 0;
        int petla = 0;
        int operacja = 0;

        Scanner klawiatura = new Scanner(System.in);

        while (petla == 0) {


            System.out.println("Wprowadź pierwszą liczbę");
            liczba1 = klawiatura.nextInt();
            System.out.println("Wprowadź operację arytmetyczną: +, -, *, /");
            znak = klawiatura.nextInt();
            System.out.println("Wprowadź drugą liczbę");
            liczba2 = klawiatura.nextInt();

            switch (znak) {
                case 0:
                    wynik = (liczba1 + liczba2);
                    break;
                case 1:
                    wynik = (liczba1 - liczba2);
                    break;
                case 2:
                    wynik = (liczba1 * liczba2);
                    break;
                case 3:
                    wynik = (liczba1 / liczba2);
                    if (liczba1 == 0 ) {
                        System.out.println("Nie dziel przez 0!");
                    } else {
                        if (liczba2 == 0) {
                            System.out.println("Nie dziel przez 0!");
                        }
                    }
                    break;
            }

            System.out.println("Wynik wykonwanej operacj to: " + wynik);



            System.out.println("Czy chcesz powtórzyć obliczenia? (t/n)");
            operacja = klawiatura.nextInt();
            if (operacja == 1) {
                continue;
            } else {
                if (operacja == 2) {
                    break;
                }

            }

        }
    }

}
